// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var cors         = require("cors");
const fileUpload = require("express-fileupload");
const fs         = require("fs");
// IMPORTAR EXPRESS
const app        = express();

//Para servidor con ssl
const http = require("http");
const https = require("https");
// Archivo de configuracion

const server = http.createServer(app)

const PORT = 3001

// 14 niveles
app.use('/fotos-fithouse',          express.static('./../../fotos-fithouse'));

// IMPORTAR PERMISOS
app.use(cors({ origin: '*' }));
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload()); //subir archivos

// ----IMPORTAR RUTAS---------------------------------------->
// require('./routes/usuarios/index.routes')(app)
require('./routes/catalogos/index.routes')(app)


// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
server.listen(PORT, () => {
    console.log(`Servidor Corriendo en el Puerto ${PORT}`);
});
