module.exports = app => {
    const ejercicios = require('../../controllers/catalogos/ejercicios.controllers') // --> ADDED THIS
    app.post("/ejercicios.add"      ,  ejercicios.addEjercicio); 
    app.put("/ejercicios.update/:id",  ejercicios.updateEjercicio); 
    app.get("/ejercicios.list"      ,  ejercicios.getEjercicios); 

  };
  