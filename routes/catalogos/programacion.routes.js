module.exports = app => {
  const programacion = require('../../controllers/catalogos/programacion.controllers') // --> ADDED THIS
  app.post("/programacion.add"        ,  programacion.addProgramacion); 
  app.put("/programacion.eliminar/:id",  programacion.updateProgramacion); 
  app.get("/programacion"             ,  programacion.getProgramacion); 
  app.post("/programacion.dia"        ,  programacion.getProgramacionDia); 
  app.post("/programacion.usuario"    ,  programacion.getProgramacionUsuario); 
  app.post("/programacion.categoria"  ,  programacion.getProgramacionCategoria); 
  app.post("/programacion.dia.all"    ,  programacion.getProgramacionDiaAll); 
  app.post("/programacion.reservacion",  programacion.addReservacion); 
  app.post("/validar.reservacion"     ,  programacion.validarReservacion); 

};
