module.exports = app => {
    const paquetes = require('../../controllers/catalogos/paquetes.controllers') // --> ADDED THIS
    app.post("/paquetes.add"      ,  paquetes.addPaquetes); 
    app.put("/paquetes.update/:id",  paquetes.updatePaquetes); 
    app.get("/paquetes.list"      ,  paquetes.getPaquetes); 

  };
  