module.exports = app => {
  const usuarios = require('../../controllers/catalogos/usuarios.controllers') // --> ADDED THIS
  app.post("/usuarios.add"           ,  usuarios.addUsuarios); 
  app.put("/usuarios.update/:id"     ,  usuarios.updateUsuarios); 
  app.get("/usuarios.list"           ,  usuarios.getUsuarios); 
  app.post("/usuarios.validate"      ,  usuarios.validarUsuario); 
  app.get("/coachs.activos"          ,  usuarios.getCoachs); 
  app.post("/usuario.update.datos"   ,  usuarios.updateUsuariosDatos); 
  app.post("/usuario.update.delete"  ,  usuarios.updateUsuariosDatosDelete); 
  app.post("/grabar.avatar"          ,  usuarios.grabarAvatar); 
  app.post("/usuario.update.password",  usuarios.updatePassword); 
};