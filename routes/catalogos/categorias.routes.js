module.exports = app => {
    const categorias = require('../../controllers/catalogos/categorias.controllers') // --> ADDED THIS
    app.post("/categorias.add"      ,  categorias.addCategorias); 
    app.put("/categorias.update/:id",  categorias.updateCategorias); 
    app.get("/categorias.list"      ,  categorias.getCategorias); 

  };
  