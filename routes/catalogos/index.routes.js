module.exports = app => {
    require("./categorias.routes")(app)
    require("./ejercicios.routes")(app)
    require("./usuarios.routes")(app)
    require("./paquetes.routes")(app)
    require("./programacion.routes")(app)
    require("./pagos.routes")(app)

    
  };