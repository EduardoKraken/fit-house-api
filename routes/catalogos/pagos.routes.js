module.exports = app => {
  const pagos = require('../../controllers/catalogos/pagos.controllers') // --> ADDED THIS
  app.post("/pagos.add"      ,  pagos.addPago); 
  app.post("/pagos.ver"      ,  pagos.validarPago); 

  // compras uuario
  app.get("/mis.cantidad.clases/:id"      ,  pagos.getCantClasesUsuario); 
  app.get("/mis.historial.pagos/:id"      ,  pagos.getMisPagos); 

};
