const { result }  = require("lodash");
const sql     = require("../db.js");

//const constructor
const paquetes = (clases) => {};

paquetes.addPaquetes = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO paquetes( paquete, cant_clases, costo, limite_dias ) VALUES( ?, ?, ?, ? )`, [ u.paquete, u.cant_clases, u.costo, u.limite_dias ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

paquetes.updatePaquetes = ( u ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE paquetes SET paquete = ?, cant_clases = ?, costo = ?, limite_dias = ?, deleted = ?  WHERE idpaquetes = ?`,[ u.paquete, u.cant_clases, u.costo, u.limite_dias, u.deleted, u.idpaquetes ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

paquetes.getPaquetes = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM paquetes WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = paquetes;