const { result }  = require("lodash");
const sql     = require("../db.js");

//const constructor
const usuarios = (clases) => {};

usuarios.addUsuarios = ( u ) => {
  return new Promise((resolve,reject)=>{
  sql.query(`INSERT INTO usuarios( nombre_completo, correo, telefono, pass, estatus, tipo_usuario, fecha_nacimiento ) VALUES( ?,?,?,MD5(?),?,?,?)`,
    [ u.nombre_completo, u.correo, u.telefono, u.pass, 1, 3, u.fecha_nacimiento ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

usuarios.updateUsuarios = ( u, id ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE usuarios SET nombre_completo= ?, edad= ?, telefono= ?, tipo_usuario= ?, fecha_nacimiento = ?, estatus= ?, deleted = ? WHERE idusuarios = ?`,
      [ u.nombre_completo, u.edad, u.telefono, u.tipo_usuario, u.fecha_nacimiento, u.estatus, u.deleted , id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

usuarios.getUsuarios = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM usuarios WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

usuarios.existeUsuario = ( correo ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM usuarios WHERE correo = ? AND deleted = 0;`,[ correo ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res[0])
    })
  })
}

usuarios.getCoachs = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM usuarios WHERE deleted = 0 AND tipo_usuario = 2 AND estatus = 1;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

usuarios.updateUsuariosDatos = ( u ) => {
  return new Promise(( resolve, reject )=>{
    sql.query(`UPDATE usuarios SET nombre_completo= ?, edad= ?, telefono= ? WHERE idusuarios = ?`,[ u.nombre_completo, u.edad, u.telefono, u.idusuarios ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
    });
  });
};

usuarios.updateUsuariosDatosDelete = ( u ) => {
  return new Promise(( resolve, reject )=>{
    sql.query(`UPDATE usuarios SET deleted = 1 WHERE idusuarios = ?`,[ u.idusuarios ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
    });
  });
};

usuarios.updateAvatar = ( foto, idusuarios ) => {
  return new Promise(( resolve, reject )=>{
    sql.query(`UPDATE usuarios SET foto = ? WHERE idusuarios = ?`,[ foto, idusuarios ],
      (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({foto, idusuarios});
    });
  });
};

// Obtener los datos del usuario mediante el id
usuarios.getUsuarioId = ( id_usuario ) => {
  return new Promise(( resolve, reject )=>{
    sql.query(`SELECT * FROM usuarios WHERE idusuarios = ? AND deleted = 0;`, [ id_usuario ], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};

// Actualizar solo la contraseña
usuarios.updatePassword = ( id, password ) => {
  return new Promise(( resolve, reject )=>{
    sql.query(`UPDATE usuarios SET pass = ? WHERE idusuarios = ?`, [ password, id ],
      (err, res) => {
      if (err) { return reject({ message: err.sqlMessage }); }
      if (res.affectedRows == 0) {
        return reject( "usuario no encontrado" );
      }
      resolve({ id, password });
    })
  })
};

module.exports = usuarios;