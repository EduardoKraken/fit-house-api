const { result }  = require("lodash");
const sql     = require("../db.js");

//const constructor
const programacion = (clases) => {};

programacion.addProgramacion = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO programacion( idcoach, idcategorias, hora_inicio, hora_fin, dia, limite_alumnos ) VALUES( ?, ?, ?, ?, ?, ? )`, 
      [ u.idcoach, u.idcategorias, u.hora_inicio, u.hora_fin, u.dia, u.limite_alumnos ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

programacion.updateProgramacion = ( idprogramacion ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE programacion SET deleted = 1  WHERE idprogramacion = ?`,[ idprogramacion ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({idprogramacion});
	  });
	});
};

programacion.getProgramacion = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT p.*, u.nombre_completo, c.categoria AS name, c.color, CONCAT(p.dia, ' ', p.hora_inicio) AS start, CONCAT(p.dia, ' ', p.hora_fin) AS end FROM programacion p
			LEFT JOIN usuarios u ON u.idusuarios = p.idcoach
			LEFT JOIN categorias c ON c.idcategorias = p.idcategorias
			WHERE p.deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

programacion.getProgramacionDia = ( dia, idusuarios ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT p.*, c.categoria, c.detalles, c.color, u.nombre_completo, u.foto, r.idreservaciones, pp.clases_reservadas,
      CASE
        WHEN hora_inicio <= TIME(NOW()) AND hora_fin >= TIME(NOW()) THEN 'Activa'
        WHEN hora_inicio > TIME(NOW()) THEN 'Por iniciar'
        ELSE 'Vencida'
      END AS estado_clase FROM programacion p
      LEFT JOIN categorias c ON c.idcategorias = p.idcategorias
      LEFT JOIN usuarios u ON u.idusuarios = p.idcoach 
      LEFT JOIN (SELECT COUNT(*) AS clases_reservadas, idprogramacion FROM reservaciones WHERE estatus <> 3 AND deleted = 0 GROUP BY idprogramacion) pp ON pp.idprogramacion = p.idprogramacion
      LEFT JOIN (SELECT * FROM reservaciones
        WHERE idusuarios = ?
        AND estatus <> 3) r ON r.idprogramacion = p.idprogramacion
      WHERE p.deleted = 0
      AND p.dia = ?
      AND  ( limite_alumnos - IFNULL(clases_reservadas,0) ) > 0
      ORDER BY hora_inicio;`,[ idusuarios, dia ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

programacion.getProgramacionUsuario = ( dia, idusuarios ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT p.*, CONCAT(dia, ' ',hora_inicio) AS completo, TIME(NOW()), c.categoria, c.detalles, c.color, u.nombre_completo, u.foto, r.idreservaciones, pp.clases_reservadas, 
      CASE
        WHEN dia = CURRENT_DATE AND TIME(NOW()) BETWEEN hora_inicio AND hora_fin  THEN 'Corriendo'
        WHEN dia = CURRENT_DATE AND TIME(NOW()) < hora_inicio THEN 'Por iniciar'
        WHEN CONCAT(dia, ' ',hora_inicio) > NOW() THEN 'Pendiente'
        ELSE 'Terminada'
      END AS estado_clase FROM programacion p
      LEFT JOIN categorias c ON c.idcategorias = p.idcategorias
      LEFT JOIN usuarios u ON u.idusuarios = p.idcoach 
      LEFT JOIN (SELECT COUNT(*) AS clases_reservadas, idprogramacion FROM reservaciones WHERE estatus <> 3 AND deleted = 0 GROUP BY idprogramacion) pp ON pp.idprogramacion = p.idprogramacion
      LEFT JOIN (SELECT * FROM reservaciones
        WHERE idusuarios = 6
        AND estatus <> 3) r ON r.idprogramacion = p.idprogramacion
      WHERE p.deleted = 0
      AND idreservaciones IS NOT NULL
      AND dia >= CURRENT_DATE
      ORDER BY dia, hora_inicio;`,[ idusuarios, dia ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

programacion.getProgramacionCategoria = ( idusuarios, idcategorias ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT p.*, c.categoria, c.detalles, c.color, u.nombre_completo, u.foto, r.idreservaciones, pp.clases_reservadas, ( limite_alumnos - IFNULL(clases_reservadas,0) ) AS restan,
      CASE
        WHEN hora_inicio <= TIME(NOW()) AND hora_fin >= TIME(NOW()) THEN 'Activa'
        WHEN hora_inicio > TIME(NOW()) THEN 'Por iniciar'
        ELSE 'Vencida'
      END AS estado_clase FROM programacion p
      LEFT JOIN categorias c ON c.idcategorias = p.idcategorias
      LEFT JOIN usuarios u ON u.idusuarios = p.idcoach 
      LEFT JOIN (SELECT COUNT(*) AS clases_reservadas, idprogramacion FROM reservaciones WHERE estatus <> 3 AND deleted = 0 GROUP BY idprogramacion) pp ON pp.idprogramacion = p.idprogramacion
      LEFT JOIN (SELECT * FROM reservaciones
        WHERE idusuarios = ?
        AND estatus <> 3) r ON r.idprogramacion = p.idprogramacion
      WHERE p.deleted = 0
      AND p.dia > CURRENT_DATE
      AND  ( limite_alumnos - IFNULL(clases_reservadas,0) ) > 0
      AND c.idcategorias = ?
      ORDER BY p.dia, hora_inicio;`,[ idusuarios, idcategorias ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

programacion.getProgramacionDiaAll = ( dia ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT p.*, c.categoria, c.detalles, c.color, u.nombre_completo, u.foto, r.idreservaciones,
      CASE
        WHEN hora_inicio <= TIME(NOW()) AND hora_fin >= TIME(NOW()) THEN 'Activa'
        WHEN hora_inicio > TIME(NOW()) THEN 'Por iniciar'
        ELSE 'Vencida'
      END AS estado_clase FROM programacion p
      LEFT JOIN categorias c ON c.idcategorias = p.idcategorias
      LEFT JOIN usuarios u ON u.idusuarios = p.idcoach 
      LEFT JOIN (SELECT * FROM reservaciones
        WHERE estatus <> 3) r ON r.idprogramacion = p.idprogramacion
      WHERE p.deleted = 0
      AND p.dia = ?
      ORDER BY hora_inicio;`,[  dia ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

programacion.addReservacion = ( idpago, idusuarios, idprogramacion ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO reservaciones( idpago, idusuarios, idprogramacion ) VALUES( ?, ?, ? )`, 
      [ idpago, idusuarios, idprogramacion ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, idpago, idusuarios, idprogramacion})
    })
  })
}


programacion.validarReservacion = ( idprogramacion, idusuarios ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM reservaciones 
      WHERE idprogramacion = ?
      AND idusuarios = ? 
      AND deleted = 0
      AND estatus <> 3;`, 
      [ idprogramacion, idusuarios],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}


module.exports = programacion;