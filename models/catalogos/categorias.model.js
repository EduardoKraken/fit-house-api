const { result }  = require("lodash");
const sql     = require("../db.js");

//const constructor
const categorias = (clases) => {};

categorias.addCategorias = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO categorias( categoria, detalles, color ) VALUES( ?, ?, ? )`, [ u.categoria, u.detalles, u.color.hex ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

categorias.updateCategorias = ( u ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE categorias SET categoria = ?, deleted = ?, detalles = ?, color = ?  WHERE idcategorias = ?`,[ u.categoria, u.deleted, u.detalles, u.color.hex, u.idcategorias ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

categorias.getCategorias = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM categorias WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = categorias;