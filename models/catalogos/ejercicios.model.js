const { result }  = require("lodash");
const sql     = require("../db.js");

//const constructor
const ejercicios = (clases) => {};

ejercicios.addEjercicio = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO ejercicios( ejercicio, idcategorias, link ) VALUES( ?, ?, ? )`, [ u.ejercicio, u.idcategorias, u.link ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

ejercicios.updateEjercicio = ( u ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE ejercicios SET ejercicio = ?, idcategorias = ?, link = ?, deleted = ?  WHERE idejercicios = ?`,
	  	[ u.ejercicio, u.idcategorias, u.link, u.deleted, u.idejercicios ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

ejercicios.getEjercicios = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT e.*, c.categoria FROM ejercicios e
			LEFT JOIN categorias c ON c.idcategorias = e.idcategorias
			WHERE e.deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = ejercicios;