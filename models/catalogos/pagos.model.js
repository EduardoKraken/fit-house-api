const { result }  = require("lodash");
const sql     = require("../db.js");

//const constructor
const paquetes = (clases) => {};

paquetes.addPaquetes = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO pagos( idusuarios, idpaquetes, cant_clases, costo, limite_dias ) VALUES( ?, ?, ?, ?, ? )`, [ u.idusuarios, u.idpaquetes, u.cant_clases, u.amount, u.limite_dias ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

paquetes.udatePago = ( estatus, idopenpay, url_pago, id ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE pagos SET estatus = ?, idopenpay = ?, url_pago = ? WHERE idpagos = ?`,[ estatus, idopenpay, url_pago, id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({ estatus, idopenpay, url_pago, id });
	  });
	});
};

paquetes.updateEstatusPago = ( status, id )  => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE pagos SET estatus = ? WHERE idpagos > 0 AND idopenpay = ?;`,[ status, id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve({ status, id });
	  });
	});
};

paquetes.getPaquetes = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM paquetes WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

paquetes.getCantClasesUsuario  = ( idusuarios ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT SUM( cant_clases ) - (SELECT COUNT(*) FROM reservaciones
      WHERE idusuarios = ?
      AND estatus <> 3 ) AS totalClases
      FROM pagos 
      WHERE idusuarios = ?
      AND estatus = "completed"
      AND deleted = 0
      ORDER BY fecha_creacion DESC`,[ idusuarios, idusuarios ], (err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res[0])
    })
  })
}

paquetes.getMisPagos  = ( idusuarios ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT p.*, DATE_ADD(DATE(p.fecha_creacion), INTERVAL limite_dias DAY) AS limite, ( p.cant_clases - ( SELECT COUNT(*) FROM reservaciones
      WHERE idusuarios = ? 
      AND estatus <> 3 AND idpago = p.idpagos)) AS cant_clases_restantes, DATE(p.fecha_creacion) AS fecha_pago FROM pagos p
      WHERE p.idusuarios = ? 
      AND p.deleted = 0
      ORDER BY p.fecha_creacion DESC;`,[ idusuarios, idusuarios ], (err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

module.exports = paquetes;