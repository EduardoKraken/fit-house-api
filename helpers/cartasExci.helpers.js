const config     = require("../config/index.js");

//const constructor
const helperCartaExci = {

	crearCarta ( calif1, calif2 ) {
		// validar la calificación que sacaron 
		let diagnostico = calif1 ? calif1.calificacion : 'No hay registro'
		let final       = calif2 ? calif2.calificacion : 'No hay registro'
		
		let carta = ''


		if( calif1 && calif2 ){
			if( diagnostico > final ){ final = diagnostico }
			if( final >= 70 ){
				carta = `Hola, le hacemos llegar el reporte de aprovechamiento de su curso EXCI. Antes que nada, reiteramos nuestros mejores deseos para el día del examen. Le deseamos el mayor de los éxitos y que todas sus metas estudiantiles sean cumplidas y siguiendo las indicaciones que le proporcionaremos estaremos seguros que así será.

Su calificación al iniciar el curso fue de: ${ diagnostico }
Su calificación al finalizar el curso fue de: ${ final }

Dictamen: Su examen está muy bien, su progreso fue notorio. Tomando en cuenta que el pase del examen es 50 puntos para titulación, usted no tendrá mayores inconvenientes para acreditar el examen (65 puntos para negocios Internacionales). Recordemos que estamos buscando obtener más de 80 puntos y con los consejos que le daremos a continuación estamos seguros que lo lograra. Los errores que se cometieron fueron muy pequeños en la parte de las reglas, sin embargo, repasándolas podrá arreglar esos pequeños detalles. El enemigo principal dentro del examen serán los nervios, le recomendamos tomar la evaluación con tranquilidad, cuenta con el conocimiento necesario y solo debemos evitar cometer errores por dudar de sus habilidades. Nuevamente felicidades y mucho éxito. 
`
			}else{
				carta = `Hola, le hacemos llegar el reporte de aprovechamiento de su curso EXCI. Antes que nada, reiteramos nuestros mejores deseos para el día del examen. Le deseamos el mayor de los éxitos y que todas sus metas estudiantiles sean cumplidas y siguiendo las indicaciones que le proporcionaremos estaremos seguros que así será.

Su calificación al iniciar el curso fue de: ${ diagnostico }
Su calificación al finalizar el curso fue de: ${ final }

Dictamen: Logro mejorar sustancialmente referente al examen de diagnóstico. Recuerde que el pase para titulación se obtiene con 50 puntos lo que nos da un margen de conocimiento (Excepto Negocios Internacionales que ocupa 65 puntos). Para que nos puedan dar el certificado el puntaje requerido es mayor a 80, el cual si hace lo que le indicaremos a continuación será capaz de lograr. Debemos reforzar las reglas gramaticales ya que es ahí donde se obtendrán muchos más puntos. Solo es cuestión de repasar lo visto en clase y podrá alcanzar su objetivo de acreditar. En la parte de audios su desempeño fue bueno, solo debemos prestar más atención el día del examen y no permitir que los nervios aparezcan y nos vayan a hacer cometer errores. Recuerde que el examen está basado en sistema de cognados, así que pensar en español durante este examen siempre será algo que aporte.
`
			}

		}


		if( calif1 && !calif2 ){
			if( diagnostico >= 70 ){
				carta = `Hola, le hacemos llegar el reporte de aprovechamiento de su curso EXCI. Antes que nada, reiteramos nuestros mejores deseos para el día del examen. Le deseamos el mayor de los éxitos y que todas sus metas estudiantiles sean cumplidas y siguiendo las indicaciones que le proporcionaremos estaremos seguros que así será.

Su calificación al iniciar el curso fue de: ${ diagnostico }
Su calificación al finalizar el curso fue de: No hay registro 

Dictamen: Lamentablemente no pudo tomar el examen de simulación, lo cual nos imposibilita para poderle ofrecer un resultado que le ayude a determinar sus áreas de oportunidad de manera eficiente. Le pedimos por favor se ponga en contacto con nosotros para ver una solución posible y pueda retomar su curso en otras fechas. Para nosotros lo más importante son ustedes y su conocimiento. Siéntase en la libertad de contactarnos para poder ayudarle. `
			}else{
				carta = `Hola, le hacemos llegar el reporte de aprovechamiento de su curso EXCI. Antes que nada, reiteramos nuestros mejores deseos para el día del examen. Le deseamos el mayor de los éxitos y que todas sus metas estudiantiles sean cumplidas y siguiendo las indicaciones que le proporcionaremos estaremos seguros que así será.

Su calificación al iniciar el curso fue de: ${ diagnostico }
Su calificación al finalizar el curso fue de: No hay registro 

Dictamen: Lamentablemente no pudo tomar el examen de simulación, lo cual nos imposibilita para poderle ofrecer un resultado que le ayude a determinar sus áreas de oportunidad de manera eficiente. Le pedimos por favor se ponga en contacto con nosotros para ver una solución posible y pueda retomar su curso en otras fechas. Para nosotros lo más importante son ustedes y su conocimiento. Siéntase en la libertad de contactarnos para poder ayudarle. `
			}

		}


		if( !calif1 && calif2 ){
			if( final >= 70 ){
				carta = `Hola, le hacemos llegar el reporte de aprovechamiento de su curso EXCI. Antes que nada, reiteramos nuestros mejores deseos para el día del examen. Le deseamos el mayor de los éxitos y que todas sus metas estudiantiles sean cumplidas y siguiendo las indicaciones que le proporcionaremos estaremos seguros que así será.

Su calificación al iniciar el curso fue de: No tomó el examen
Su calificación al finalizar el curso fue de: ${ final }

Dictamen: Su examen está muy bien, su progreso fue notorio. Tomando en cuenta que el pase del examen es 50 puntos para titulación, usted no tendrá mayores inconvenientes para acreditar el examen (65 puntos para negocios Internacionales). Recordemos que estamos buscando obtener más de 80 puntos y con los consejos que le daremos a continuación estamos seguros que lo lograra. Los errores que se cometieron fueron muy pequeños en la parte de las reglas, sin embargo, repasándolas podrá arreglar esos pequeños detalles. El enemigo principal dentro del examen serán los nervios, le recomendamos tomar la evaluación con tranquilidad, cuenta con el conocimiento necesario y solo debemos evitar cometer errores por dudar de sus habilidades. Nuevamente felicidades y mucho éxito. 
`
			}else{
				carta = `Hola, le hacemos llegar el reporte de aprovechamiento de su curso EXCI. Antes que nada, reiteramos nuestros mejores deseos para el día del examen. Le deseamos el mayor de los éxitos y que todas sus metas estudiantiles sean cumplidas y siguiendo las indicaciones que le proporcionaremos estaremos seguros que así será.

Su calificación al iniciar el curso fue de: No hay registro
Su calificación al finalizar el curso fue de: ${ final }

Dictamen: Lamentablemente no pudo tomar su examen de diagnóstico. Recuerde que el pase para titulación se obtiene con 50 puntos lo que nos da un margen de conocimiento (Excepto Negocios Internacionales que ocupa 65 puntos). Para que nos puedan dar el certificado el puntaje requerido es mayor a 80, el cual si hace lo que le indicaremos a continuación será capaz de lograr. Debemos reforzar las reglas gramaticales ya que es ahí donde se obtendrán muchos más puntos. Solo es cuestión de repasar lo visto en clase y podrá alcanzar su objetivo de acreditar. En la parte de audios su desempeño fue bueno, solo debemos prestar más atención el día del examen y no permitir que los nervios aparezcan y nos vayan a hacer cometer errores. Recuerde que el examen está basado en sistema de cognados, así que pensar en español durante este examen siempre será algo que aporte.`
			}

		}



	  return carta
	},
};


module.exports = helperCartaExci;