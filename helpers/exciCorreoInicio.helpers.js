const config     = require("../config/index.js");
const nodemailer = require('nodemailer');


//const constructor
const helperExciBienvenida = {

	enviarFolioBienvenida ( folio ) {
		const correo = `
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
				<head>
					<!--[if gte mso 9]>
					<xml>
						<o:OfficeDocumentSettings>
						<o:AllowPNG/>
						<o:PixelsPerInch>96</o:PixelsPerInch>
						</o:OfficeDocumentSettings>
					</xml>
					<![endif]-->
					<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
				    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
					<meta name="format-detection" content="date=no" />
					<meta name="format-detection" content="address=no" />
					<meta name="format-detection" content="telephone=no" />
					<meta name="x-apple-disable-message-reformatting" />
				    <!--[if !mso]><!-->
				   	<link href="https://fonts.googleapis.com/css?family=Kreon:400,700|Playfair+Display:400,400i,700,700i|Raleway:400,400i,700,700i|Roboto:400,400i,700,700i" rel="stylesheet" />
				    <!--<![endif]-->
					<title>EXCI REGISTRO</title>
					<!--[if gte mso 9]>
					<style type="text/css" media="all">
						sup { font-size: 100% !important; }
					</style>
					<![endif]-->
					

					<style type="text/css" media="screen">
						/* Linked Styles */
						body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#1e52bd; -webkit-text-size-adjust:none }
						a { color:#000001; text-decoration:none }
						p { padding:0 !important; margin:0 !important } 
						img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
						.mcnPreviewText { display: none !important; }
						.text-footer2 a { color: #ffffff; } 
						
						/* Mobile styles */
						@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
							.mobile-shell { width: 100% !important; min-width: 100% !important; }
							
							.m-center { text-align: center !important; }
							.m-left { text-align: left !important; margin-right: auto !important; }
							
							.center { margin: 0 auto !important; }
							.content2 { padding: 8px 15px 12px !important; }
							.t-left { float: left !important; margin-right: 30px !important; }
							.t-left-2  { float: left !important; }
							
							.td { width: 100% !important; min-width: 100% !important; }

							.content { padding: 30px 15px !important; }
							.section { padding: 30px 15px 0px !important; }

							.m-br-15 { height: 15px !important; }
							.mpb5 { padding-bottom: 5px !important; }
							.mpb15 { padding-bottom: 15px !important; }
							.mpb20 { padding-bottom: 20px !important; }
							.mpb30 { padding-bottom: 30px !important; }
							.mp30 { padding-bottom: 30px !important; }
							.m-padder { padding: 0px 15px !important; }
							.m-padder2 { padding-left: 15px !important; padding-right: 15px !important; }
							.p70 { padding: 30px 0px !important; }
							.pt70 { padding-top: 30px !important; }
							.p0-15 { padding: 0px 15px !important; }
							.p30-15 { padding: 30px 15px !important; }			
							.p30-15-0 { padding: 30px 15px 0px 15px !important; }			
							.p0-15-30 { padding: 0px 15px 30px 15px !important; }			


							.text-footer { text-align: center !important; }

							.m-td,
							.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

							.m-block { display: block !important; }

							.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

							.column,
							.column-dir,
							.column-top,
							.column-empty,
							.column-top-30,
							.column-top-60,
							.column-empty2,
							.column-bottom { float: left !important; width: 100% !important; display: block !important; }

							.column-empty { padding-bottom: 15px !important; }
							.column-empty2 { padding-bottom: 30px !important; }

							.content-spacing { width: 15px !important; }
						}
					</style>
				</head>
				<body class="body"style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#1e52bd; -webkit-text-size-adjust:none;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1e52bd">
						<tr>
							<td align="center" valign="top">
								<!-- Main -->
								<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
									<tr>
										<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
											<!-- Header -->
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td class="p30-15" style="padding: 20px 0px 20px 0px;">
													</td>
												</tr>
												<!-- END Top bar -->
												<!-- Logo -->
												<tr>
													<td bgcolor="#ffffff" class="p30-15 img-center" style="padding: 30px; border-radius: 20px 20px 0px 0px; font-size:0pt; line-height:0pt; text-align:center;"><a href="#" target="_blank"><img src="https://fastenglish.com.mx/public/images/logo.png" width="115" height="100" mc:edit="image_6" style="max-width:146px;" border="0" alt="" /></a></td>
												</tr>
												<!-- END Logo -->
											</table>
											<!-- END Header -->
												
											<!-- Section 1 -->
											<div mc:repeatable="Select" mc:variant="Section 1">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
													<tr>
														<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://fastenglish.com.mx/public/images/hero2.jpg" width="650" height="358" mc:edit="image_7" style="max-width:650px;" border="0" alt="" /></td>
													</tr>
													<tr>
														<td class="p30-15-0" style="padding: 50px 30px 0px;" bgcolor="#ffffff">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="h5-center"style="color:#a1a1a1; font-family:'Raleway', Arial,sans-serif; font-size:16px; line-height:22px; text-align:center; padding-bottom:5px;">
																		<div mc:edit="text_3">Bienvenido</div>
																	</td>
																</tr>
																<tr>
																	<td class="h2-center"style="color:#000000; font-family:'Playfair Display', Times, 'Times New Roman', serif; font-size:32px; line-height:36px; text-align:center; padding-bottom:20px;">
																		<div mc:edit="text_4">${ folio }</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</div>
											<!-- END Section 1 -->

											<!-- Section 2 -->
											<div mc:repeatable="Select" mc:variant="Section 2">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td>
															<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#dde8fd">
																<tr>
																	<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://fastenglish.com.mx/public/images/free_white_blue.jpg" width="650" height="162" mc:edit="image_8" style="max-width:650px;" border="0" alt="" /></td>
																</tr>
																<tr>
																	<td class="p0-15" style="padding: 0px 30px;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="h2-center"style="color:#000000; font-family:'Playfair Display', Times, 'Times New Roman', serif; font-size:32px; line-height:36px; text-align:center; padding-bottom:20px;">
																					<div mc:edit="text_6">Formas de pago</div>
																				</td>
																			</tr>

																			<tr>
																				<td class="h5-center"style="font-family:'Raleway', Arial,sans-serif; font-size:16px; line-height:22px; text-align:center; padding-bottom:25px;">
																					<div mc:edit="text_3">Existen 2 formas de realizar las separación o liquidación de tu beca las cuales se detallarán a continuación.  </div>
																				</td>
																			</tr>

																			<tr>
																				<td class="pb40"style="padding-bottom:40px;">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td class="event-separator"style="padding-bottom:40px; border-bottom:1px solid #ffffff;">
																								<table width="100%" border="0" cellspacing="0" cellpadding="0">
																									<tr>
																										<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																										<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																											<table width="100%" border="0" cellspacing="0" cellpadding="0">
																												<tr>
																													<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;"><div mc:edit="text_10">Obtener tu beca ahora</div></td>
																												</tr>
																												<tr>
																													<td class="text black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:left; color:#000000;">
																														<div mc:edit="text_11">
																															Este método asegura tu lugar dentro del programa de becas para realizar la separación o liquidación de tu beca solo deberás acudir con la asesora de FAST ENGLISH y mencionarle tu folio, las formas de pago son las siguientes: 

																															<ul>
																															  <li>Pago en efectivo</li>
																															  <li>Pago con tarjeta</li>
																															</ul>  

																														</div>
																													</td>
																												</tr>
																											</table>
																										</th>
																										<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>

																			<tr>
																				<td class="pb40"style="padding-bottom:40px;">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																							<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																								<table width="100%" border="0" cellspacing="0" cellpadding="0">
																									<tr>
																										<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;"><div mc:edit="text_15">Obtener tu beca después</div></td>
																									</tr>
																									<tr>
																										<td class="text black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:left; color:#000000;">
																											<div mc:edit="text_16">
																												Este método está sujeto a disponibilidad de becas, si se acaban los lugares antes de realizar el pago, ya no se podrá obtener el beneficio, así que te recomendamos realizar tu separación cuanto antes. Las formas de pago son las siguientes: 

																												<!-- Efectivo -->
																												<ul>
																												  <li>Pago en efectivo</li>
																													<li>Pago con tarjeta</li>
																												</ul>

																												Encuentra la sucursal más cercana a ti.
																												<br/>
																												<a href="https://fastenglish.com.mx/" target="_blank"><b>Sucursales</b></a>
																												<br/>
																												<br/>

																												<!-- Depositos -->
																												<ul>
																												  <li>Depósito</li>
																												  <li>Transferencia</li>
																												  <li>Pago en OXXO o Seven</li>
																												</ul>  
																												<b>* Las cuentas se encuentran en el archivo anexado a este mismo correo *</b><br/>
																												Por favor, envía tu comprobante de pago al siguiente correo y en asunto deberás ingresar tu folio asignado.
																												<br/>
																												<b>exci@fastenglish.com.mx</b>
																												<br/>
																												<br/>

																												<!-- Pago en línea -->
																												<ul>
																												  <li>Pago en línea</li>
																												</ul>
																												También puedes realizar tu pago en línea a través de la siguiente plataforma:
																												<br/>
																												<a href="https://qa.fastenglish.com.mx/procesarpago.php" target="_blank"><b>Pago en línea</b></a>
																												<br/>
																												Deberás seleccionar la opción de "ALUMNO NUEVO" e ingresar tu folio asignado.

																											</div>
																										</td>
																									</tr>
																								</table>
																							</th>
																							<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																						</tr>
																					</table>
																				</td>
																			</tr>

																			<tr>
																				<td class="pb40"style="padding-bottom:40px;">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																							<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																								<table width="100%" border="0" cellspacing="0" cellpadding="0">
																									<tr>
																										<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;"><div mc:edit="text_15">Para dudas o aclaraciones</div></td>
																									</tr>
																									<tr>
																										<td class="text black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:left; color:#000000;">
																											<div mc:edit="text_16">
																												Envía un WhatsApp a: (81) 2567 9723
																											</div>
																										</td>
																									</tr>
																								</table>
																							</th>
																							<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://fastenglish.com.mx/public/images/free_blue_white.jpg" width="650" height="160" mc:edit="image_9" style="max-width:650px;" border="0" alt="" /></td>
													</tr>
												</table>
											</div>
											<!-- END Section 2 -->
											
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td class="text-footer2 p30-15" style="padding: 30px 15px 50px 15px; color:#a9b6e0; font-family:'Raleway', Arial,sans-serif; font-size:12px; line-height:22px; text-align:center;">
													</td>
												</tr>
											</table>
											<!-- END Footer -->
										</td>
									</tr>
								</table>
								<!-- END Main -->

							</td>
						</tr>
					</table>
				</body>
				</html>

		`
	  return correo
	},
};


module.exports = helperExciBienvenida;