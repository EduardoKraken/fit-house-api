const usuarios   = require("../../models/catalogos/usuarios.model.js");
const md5 = require('md5');
const { v4: uuidv4 } = require('uuid')


// Enviar el recibo de pago al alumno
exports.addUsuarios = async(req, res) => {
  try {

    const { correo } = req.body

    // BUscar primero si el suaurio que se está dando de alta ya existe en el abase de datos
    // si es así, hay que retornar un error de que el usuario ya existe
    const existeUsuario = await usuarios.existeUsuario( correo ).then(response => response) 

    if( existeUsuario ){
      return res.status( 500 ).send({ message: 'El usuario que intentas agregar ya existe.' })
    }

    const usuarioAgregado  = await usuarios.addUsuarios( req.body ).then( response => response )

    res.send({message: 'Exito en el proceso' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateUsuarios= async(req, res) => {
  try {

    const { id } = req.params

    const usuario   = await usuarios.updateUsuarios( req.body, id ).then( response => response )

    res.send( usuario );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getUsuarios = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const usuario = await usuarios.getUsuarios( ).then( response=> response ) 
      
    res.send( usuario );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.validarUsuario = async(req, res) => {
  try {

    const { correo, pass } = req.body

    // BUscar primero si el suaurio que se está dando de alta ya existe en el abase de datos
    // si es así, hay que retornar un error de que el usuario ya existe
    const existeUsuario = await usuarios.existeUsuario( correo ).then(response => response) 

    if(!existeUsuario ){
      return res.status( 500 ).send({ message: 'El usuario no existe, intenta nuevamente.' })
    }

    if( existeUsuario.pass != md5(pass) ){
      return res.status( 500 ).send({ message: 'La contraseña es incorrecta.' })
    }

    res.send(existeUsuario);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getCoachs = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const usuario = await usuarios.getCoachs( ).then( response=> response ) 
      
    res.send( usuario );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateUsuariosDatos= async(req, res) => {
  try {


    const usuario   = await usuarios.updateUsuariosDatos( req.body ).then( response => response )

    res.send( usuario );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateUsuariosDatosDelete= async(req, res) => {
  try {

    const usuario   = await usuarios.updateUsuariosDatosDelete( req.body ).then( response => response )

    res.send( usuario );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.grabarAvatar= async(req, res) => {
  try {

    if( !req.files ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' })
    }
    
    // desestrucutramos los arvhios cargados
    const { file } = req.files
    const { idusuarios } = req.body

    const nombreSplit = file.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
    // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo
    
    // extensiones validas
    let extensiones  = [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'PNG', 'JPG', 'JPEG', 'gif', 'bmp' ]
    let ruta         = './../../fotos-fithouse/' + nombreUuid

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo) )
      return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
    

    file.mv(`${ruta}`, async err => {
      if (err) {
        return res.status(400).send({ message: err });
      }

      try {
        const usuario = await usuarios.updateAvatar(nombreUuid, idusuarios);
        return res.send({ message: 'Imagen cargada correctamente', nombre: nombreUuid, extension: extensioArchivo });
      } catch (error) {
        return res.status(500).send({ message: 'Error al actualizar el avatar del usuario' });
      }
    });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.updatePassword = async (req, res) => {
  try{
    // Sacamos el id - Recibe un parametro encriptado con base64
    const { idusuarios, password, newpassword } = req.body

    const usuario = await usuarios.getUsuarioId( idusuarios ).then( response => response )
    // Si el usuario no existe, hay que retornar que no existe
    if(!usuario){ return res.status( 400 ).send( { message: 'Usuario no existe' } ) }

    // ahora validar las contraseña recibida debe ser igual a la actual
    if(md5(password) != usuario.pass){
      return res.status( 400 ).send({ message: 'Las contraseñas no coinciden, intenta de nuevo' })
    }

    const updatePassword = await usuarios.updatePassword( idusuarios, md5(newpassword) ).then( response=> response )

    res.send({ message: 'Actualización completada' })
  }catch( error ){
    res.status( 500 ).send( { message: 'Error al actualizar los datos', error: error ? error.message : 'Error en el servidor' } )
  }
}