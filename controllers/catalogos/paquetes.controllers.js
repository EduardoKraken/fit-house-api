const paquetes   = require("../../models/catalogos/paquetes.model.js");

// Enviar el recibo de pago al alumno
exports.addPaquetes = async(req, res) => {
  try {

    const categoria   = await paquetes.addPaquetes( req.body ).then( response => response )

    res.send({message: 'Exito en el proceso' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updatePaquetes = async(req, res) => {
  try {

    const { id } = req.params

    const categoria   = await paquetes.updatePaquetes( req.body, id ).then( response => response )

    res.send( categoria );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getPaquetes = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const categoria = await paquetes.getPaquetes( ).then( response=> response ) 
      
    res.send( categoria );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
