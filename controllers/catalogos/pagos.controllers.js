const pagos   = require("../../models/catalogos/pagos.model.js");
var Openpay = require('openpay');

//instantiation
var openpay = new Openpay('mb0d3f0xwn6jqsd6g7e9', 'sk_df6683b8c5ef477cadaecc1f09bb463f' );
openpay.setProductionReady(true);
//use the api

// Enviar el recibo de pago al alumno
exports.addPago = async(req, res) => {
  try {

	  console.log( req.body )
  	
  	// Generar la orden de pedido
    const orden   = await pagos.addPaquetes( req.body ).then( response => response )

    // Crear el folio 
    let order_id = 'FITHOUSE-' + orden.id

  	// Generar el pedido
    const { amount, description, customer, idusuarios } = req.body

    // Creamos los datos del pago
    var chargeRequest = {
	   'method' : 'card',
	   amount,
	   description,
	   order_id,
	   customer,
	  'send_email' : false,
	  'confirm' : false,
	  'use_3d_secure': true,
	  'redirect_url' : 'https://sofsolution.com/fit-house/procesarpago/'
	}

	// // console.log( chargeRequest )

	openpay.charges.create(chargeRequest, async function(error, charge) {
		if( error ){
			return res.status(500).send({ message: error.description })
		}

		// Sacamos los datos necesarios para actualizar el pedido
		const { status, id, payment_method } = charge
		const { url } = payment_method

		// Actualizamos el pago
    const updatePago   = await pagos.udatePago( status, id, url, orden.id ).then( response => response )

    // Envíamos la respuesta
    res.send(charge);
	});


  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.validarPago = async(req, res) => {
  try {
    const { transactionId } = req.body;

    openpay.charges.list(function(error, chargeList) {
      if (error) {
        console.error("Error al obtener la lista de cargos:", error);
        return res.status(500).send({ message: error.description || 'Ocurrió un error al obtener la lista de cargos.' });
      }

      try {
        const existePago = chargeList.find(el => el.id === transactionId);

        if (existePago) {
          const { status, id } = existePago;
          // hay que actualizar el pago
          pagos.updateEstatusPago(status, id)
            .then(updateEstatusPago => {
              console.log(updateEstatusPago);
              return res.send(existePago);
            })
            .catch(updateError => {
              console.error("Error al actualizar el estatus del pago:", updateError);
              return res.status(500).send({ message: 'Ocurrió un error al actualizar el estatus del pago.' });
            });
        } else {
          return res.status(500).send({ message: 'El pago no existe' });
        }
      } catch (err) {
        console.error("Error en la solicitud de lista de cargos:", err);
        return res.status(500).send({ message: 'Ocurrió un error al procesar la solicitud.' });
      }
    });
  } catch (error) {
    console.error("Error en la función validarPago:", error);
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' });
  }
};


exports.updatePaquetes = async(req, res) => {
  try {

    const { id } = req.params

    const categoria   = await pagos.updatePaquetes( req.body, id ).then( response => response )

    res.send( categoria );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getPaquetes = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const categoria = await pagos.getPaquetes( ).then( response=> response ) 
      
    res.send( categoria );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.getCantClasesUsuario = async(req, res) => {
  try {

    const { id } = req.params 

    // REQUISICIÓN DE COMPRA
    const cantClases = await pagos.getCantClasesUsuario( id ).then( response=> response ) 

    let cantidadClases = {
      cantidadClases: cantClases ? cantClases.totalClases : 0
    }
      
    res.send( cantidadClases );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getMisPagos = async(req, res) => {
  try {

    const { id } = req.params 

    // REQUISICIÓN DE COMPRA
    const historialPagos = await pagos.getMisPagos( id ).then( response=> response ) 

    res.send( historialPagos );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};