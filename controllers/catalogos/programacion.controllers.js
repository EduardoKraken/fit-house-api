const programacion   = require("../../models/catalogos/programacion.model.js");

// Enviar el recibo de pago al alumno
exports.addProgramacion = async(req, res) => {
  try {

    const ejercicio   = await programacion.addProgramacion( req.body ).then( response => response )

    res.send({message: 'Exito en el proceso' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateProgramacion= async(req, res) => {
  try {

    const { id } = req.params

    const ejercicio   = await programacion.updateProgramacion( id ).then( response => response )

    res.send( ejercicio );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getProgramacion = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const progra = await programacion.getProgramacion( ).then( response=> response ) 
      
    res.send( progra );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.getProgramacionDia = async(req, res) => {
  try {

    const { dia, idusuarios } = req.body

    // REQUISICIÓN DE COMPRA
    const progra = await programacion.getProgramacionDia( dia, idusuarios ).then( response=> response ) 
      
    res.send( progra );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.getProgramacionUsuario = async(req, res) => {
  try {

    const { dia, idusuarios } = req.body

    // REQUISICIÓN DE COMPRA
    const progra = await programacion.getProgramacionUsuario( dia, idusuarios ).then( response=> response ) 
      
    res.send( progra );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getProgramacionCategoria = async(req, res) => {
  try {

    const { idusuarios, idcategorias } = req.body

    // REQUISICIÓN DE COMPRA
    const progra = await programacion.getProgramacionCategoria( idusuarios, idcategorias ).then( response=> response ) 
      
    res.send( progra );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getProgramacionDiaAll = async(req, res) => {
  try {

    const { dia } = req.body

    // REQUISICIÓN DE COMPRA
    const progra = await programacion.getProgramacionDiaAll( dia ).then( response=> response ) 
      
    res.send( progra );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.addReservacion = async(req, res) => {
  try {

    const { clases, idpago, idusuarios } = req.body

    for( const i in clases ){

      const { idprogramacion } = clases[i]

      // REQUISICIÓN DE COMPRA
      const progra = await programacion.addReservacion( idpago, idusuarios, idprogramacion ).then( response=> response ) 


    }
      
    res.send({ message: 'Clases reservadas'});

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.validarReservacion = async(req, res) => {
  try {

    const { idusuarios, idprogramacion } = req.body

    // REQUISICIÓN DE COMPRA
    const progra = await programacion.validarReservacion( idprogramacion, idusuarios ).then( response=> response ) 

    if( progra ){
      return res.send( progra );
    }else{
      return res.status(500).send({ message: 'No cuentas con una reservación en está clase'})
    }

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
